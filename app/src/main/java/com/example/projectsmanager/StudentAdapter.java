package com.example.projectsmanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder> {

   private Context context;
   private ArrayList stdnameList,stdPasswordList,stdIdList;

    StudentAdapter(Context context, ArrayList stdnameList,ArrayList stdPasswordList,ArrayList stdIdList){
        this.context=context;
        this.stdnameList=stdnameList;
        this.stdPasswordList=stdPasswordList;
        this.stdIdList=stdIdList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(context);
       View view= inflater.inflate(R.layout.my_row,parent,false);
       return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.stdName.setText(String.valueOf(stdnameList.get(position)));
        holder.stdPassword.setText(String.valueOf(stdPasswordList.get(position)));
        holder.stdId.setText(String.valueOf(stdIdList.get(position)));


    }

    @Override
    public int getItemCount() {
        return stdIdList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView stdName,stdPassword,stdId;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            stdName=itemView.findViewById(R.id.stdNameRow);
            stdPassword=itemView.findViewById(R.id.stdPasswordRow);
            stdId=itemView.findViewById(R.id.stdIdRow);

        }
    }
}

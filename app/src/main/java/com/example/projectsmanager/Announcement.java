package com.example.projectsmanager;

public class Announcement {
    String Id,description;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Announcement() {
    }

    public Announcement(String id, String description) {
        Id = id;
        this.description = description;
    }
}

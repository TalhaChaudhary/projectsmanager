package com.example.projectsmanager;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PostAnnouncementActivity extends AppCompatActivity {

    EditText description;
    Button post;
    DatabaseReference reference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_announcement);
        description=findViewById(R.id.descriptionText);
        post=findViewById(R.id.okay_post);
        Bundle b=getIntent().getExtras();
        //getting extras from intent
        final String groupId=b.getString("groupId");

        reference= FirebaseDatabase.getInstance().getReference("Announcement").child(groupId);
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
                String format = simpleDateFormat.format(new Date());
                Announcement a=new Announcement(format,description.getText().toString());
                reference.child(groupId+format).setValue(a);
                Intent i=new Intent(PostAnnouncementActivity.this,AnnouncementActivity.class);
                i.putExtra("groupId",groupId);
                i.putExtra("type","teacher");
                startActivity(i);
                finish();
            }
        });
    }
}
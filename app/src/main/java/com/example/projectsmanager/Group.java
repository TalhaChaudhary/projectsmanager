package com.example.projectsmanager;

public class Group {
    public String id, name, stdList;
    public Group(){
    }
    public Group(String id, String name, String stdList) {
        this.id = id;
        this.name = name;
        this.stdList = stdList;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStdList() {
        return stdList;
    }

    public void setStdList(String stdList) {
        this.stdList = stdList;
    }
}

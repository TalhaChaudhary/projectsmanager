package com.example.projectsmanager;

import java.util.Date;

public class Message {
    String message,sender;
    Date dateTime;

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public Message(String message, String sender, Date dateTime) {
        this.message = message;
        this.sender = sender;
        this.dateTime = dateTime;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}

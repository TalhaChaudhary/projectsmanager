package com.example.projectsmanager;

import android.widget.DatePicker;

import java.util.Date;

public class Assignment {
    String Id,description;
    Date dateTime;

    public Assignment(String id, String description, Date dateTime) {
        Id = id;
        this.description = description;
        this.dateTime = dateTime;
    }

    public Assignment() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}

package com.example.projectsmanager;

public class uploadedFile {
    String studentId;
    String fileURL;

    public uploadedFile() {
    }
    public String getStudentId() {
        return studentId;
    }
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }
    public String getFileURL() {
        return fileURL;
    }
    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }
    public uploadedFile(String studentId, String fileURL) {
        this.studentId = studentId;
        this.fileURL = fileURL;
    }
}

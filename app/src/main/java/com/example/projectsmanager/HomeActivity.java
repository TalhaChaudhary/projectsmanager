package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private Button add;
    private ArrayList<String> groupNameList,groupIdList;
    private RecyclerView recycle;
    private DatabaseReference reference2;
    private ArrayList<Group> gList;
    GroupAdapter groupAdapter;
    String teacherId;
    String userName;
    String gp;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Bundle b=getIntent().getExtras();
        teacherId=b.getString("teacherId");
        userName=b.getString("userName");
        type=b.getString("type");
        reference2 = FirebaseDatabase.getInstance().getReference("groups").child(teacherId);
        add = findViewById(R.id.addGroup);
        recycle = findViewById(R.id.recycler_view_home);
        groupNameList = new ArrayList<>();
        groupIdList = new ArrayList<>();
        gList= new ArrayList<>();
        reference2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    gList.add(postSnapshot.getValue(Group.class));

                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(HomeActivity.this, databaseError.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void viewGroups(View view) {

           for (int i=0;i< gList.size();i++){
            groupIdList.add(gList.get(i).getId());
            groupNameList.add(gList.get(i).getName());
        }

        groupAdapter = new GroupAdapter(HomeActivity.this, groupNameList, groupIdList,userName,type);
        recycle.setAdapter(groupAdapter);
        recycle.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
    }

    public void addGroups(View view) {
        Intent i=new Intent(HomeActivity.this, AddGroupActivity.class);
        i.putExtra("teacherId",teacherId);
        startActivity(i);
    }
}

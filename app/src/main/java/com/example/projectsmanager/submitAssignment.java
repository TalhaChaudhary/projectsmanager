package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

public class submitAssignment extends AppCompatActivity {

    TextView uploaded;
    Button upload,submit;
    DatabaseReference databaseReference;
    StorageReference storageReference;
    String groupId,suid,assignmentId;
    Uri file;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_assignment);
        upload=findViewById(R.id.upload);
        uploaded=findViewById(R.id.uploaded);
        submit=findViewById(R.id.submit);
        Bundle b=getIntent().getExtras();
        groupId=b.getString("groupId");
        suid=b.getString("suid");
        assignmentId=b.getString("assignmentId");
        if(ContextCompat.checkSelfPermission(submitAssignment.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat
                    .requestPermissions(
                            submitAssignment.this,
                            new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                            10);
            // Permission is not granted
        }
        storageReference= FirebaseStorage.getInstance().getReference();
        databaseReference= FirebaseDatabase.getInstance().getReference("uploadedFile").child(groupId);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPDFFile();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url=file.toString();
                uploadedFile f=new uploadedFile(suid,url);
                databaseReference.child(assignmentId).child(suid).setValue(f)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Intent i=new Intent(submitAssignment.this,AssignmentActivity.class);
                    i.putExtra("groupId",groupId);
                    i.putExtra("type","student");
                    i.putExtra("suid",suid);
                    startActivity(i);
                }
            })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });
            }
        });
    }

    private void selectPDFFile() {
        Intent i=new Intent();
        i.setType("application/pdf");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i,"Select pdf File...."),1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode==RESULT_OK && data!=null && data.getData()!=null)
        {
            uploadPDFFILE(data.getData());
        }
    }

    private void uploadPDFFILE(Uri data) {
        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Uploading...!!");
        progressDialog.show();
        file=data;
        StorageReference reference=storageReference.child("uploads/"+System.currentTimeMillis()+".pdf");
        final StorageTask<UploadTask.TaskSnapshot> taskSnapshotStorageTask = reference.putFile(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();
                while (!uri.isComplete()) ;
                Uri url = uri.getResult();
                progressDialog.dismiss();
                uploaded.setText("File has been uploaded Now you can Submit...!!");
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress=(100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                progressDialog.setMessage("Upload:  "+(int)progress+"%");
            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == 10) {

            // Checking whether user granted the permission or not.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // Showing the toast message
                Toast.makeText(submitAssignment.this,
                        "External Storage Permission Granted",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(submitAssignment.this,
                        "Camera Permission Denied",
                        Toast.LENGTH_SHORT)
                        .show();
            }

        }
    }

    }